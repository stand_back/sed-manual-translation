这是对[sed用户手册](https://www.gnu.org/software/sed/manual/sed.html)的翻译。  
有翻译不正确的地方请指出。

# [第二章 运行 sed](https://www.gnu.org/software/sed/manual/sed.html#Invoking-sed)

这一章介绍如何运行sed.  
关于sed 脚本和各种sed命令的具体内容 会在一下章里进行介绍。
  * 2.1  概述
  * 2.2 命令行选项
  * 2.3 退出状态

## 2.1  概述

通常sed是通过下面的形式运行的

`sed SCRIPT INPUTFILE ......`

例如：把所有的在input.txt中出现的'hello'替换成'world':

`sed 's/hello/world/' input.txt > output.txt`

如果没有指定INPUTFILE 或者 INPUTFILE是 -， sed 会对标准输入的内容进行过滤。  
下面的命令是等价的：  
`sed 's/hello/world/' input.txt > output.txt`  
`sed 's/hello/world/' < input.txt > output.txt`  
`cat input.txt | sed 's/hello/world/' - > output.txt`  

默认情况下sed 把输出写到标准输出。  
使用 -i 的情况下，会在输入文件上直接进行修改，面不是把结果输出到标准输出。  
如何把输出写到其它文件上，请参考 W 和 s///w 命令。  
下面的命令直接 修改file.txt ,并不产生额外的输出。  
`sed -i 's/hello/world/' file.txt`

默认情况下，sed会把所有的处理过的输入进行输出（除非输入被 一些命令改变修改或删除，比如 d）。  
使用 -n 可以控制 输出 ， p命令 指定要打印的行。  
下面的命令只打印输入文件的第45行：  
`sed -n '45p' file.txt`

sed把多个输入文件当做一个流进行对待，  
下面的例子打印了第一个文件（one.txt）的第一行 和 最后一个文件的（three.txt)的最后一行，  
`sed -n  '1p ; $p' one.txt two.txt three.txt`    
使用-s 可以对这个行为进行反转  

如果 没有-e 或 -f 选项， sed使用第一个非选项参数做为命令脚本，而之后的非选项参数作为输入文件。  
如果 使用 -e 或 -f 选项 指定命令脚本，所有的非选项参数 都被 当作输入文件。  
选项-e 跟 -f 可以联合起来使用， 也可以同时出现多次（在这种情况下，最终的命令脚本是所有脚本的集合）  
下面的这些例子是等价的：  
`sed 's/hello/world/' input.txt > output.txt`  
`sed -e 's/hello/world/' input.txt > output.txt`  
`sed --expression='s/hello/world/' input.txt > output.txt`  
`echo 's/hello/world/' > myscript.sed`  
`sed -f myscript.sed input.txt > output.txt`  
`sed --file=myscript.sed input.txt > output.txt`  

## 2.2 命令行选项

运行sed的完整格式如下  
`sed OPTIONS... [SCRIPT] [INPUTFILE...]`

sed运行的时候可能带有下面的这些命令行选项：

#### --verion
打印sed 版本信息， 版权信息，然后退出

#### --help
打印帮忙信息，简单的描述这些信念选项的功能 ， 显示bug返馈地址，然后退出

#### -n  
##### --quiet  
##### --silent  
默认情况下，sed会在第一个循环结束时打印出模式空间的内容。  
这些选项会禁用这个自动 打印功能 。sed只在通过p命令显式告知的时候 产生输出打印  

#### --debug
以简洁的方式打印sed程序的输入，并对程序的执行进行解释打印   
`$ echo 1 | sed '\%1%s21232'`  
`3`  
`$ echo 1 | sed --debug '\%1%s21232'`  
`SED PROGRAM:`  
`  /1/ s/1/3/`  
`INPUT:   'STDIN' line 1`  
`PATTERN: 1`  
`COMMAND: /1/ s/1/3/`  
`PATTERN: 3`  
`END-OF-CYCLE:`  
`3`  


#### -e script
##### --expression=script
把脚本里的命令加到命令集里，它们在处理输入的时候运行

#### -f script-file
##### --file=script-file
把脚本文件里的命令加到命令集里，它们在处理输入的时候运行

#### -i [SUFFIX]
##### --in-place[=SUFFIX]
这个选项指定 文件被原地修改。  
GNU sed 是通过创建一个临时文件然后把输出 发送到这个文件里 面不是标准输出。  
这个选项暗示了-s
当处理完文件，临时文件被重命名为输出文件的原始名字。  
如果指定了SUFFIX，他会在重命名临时文件前修改旧文件的名字。  
因此可以产生一个备份文件。  
修改主旧文件的名字要遵守下面的规则：  
* 如果 SUFFIX中不包含*， SUFFIX会被 放在当前文件的名的后面做为一个后缀。
* 如果 SUFFIX中包含一个或多个*， 每个星号（*） 使用当前的文件名进行替换。 这可以让你给备份文件加上前缀而不仅是后缀，或把备份文件放到另一个目录（需要提供一个已经存在的目录 ）  

如果没有SUFFIX没有指定， 原文件会被重写，不会产生备份文件。  
因为-i 会带一个可选参数， 它后面不能跟直接 的选项：
`sed -Ei '...' FILE`
   跟 -E -i一样，没有备份文件后缀 - FILE会被 直接 编辑不会产生备份文件  
`sed -iE '...' FILE`
   这个等同于 --in-place=E, 产生一个名为FILEE的FILE备份文件   
同时使用 -n 跟-i的时候 一定要小心：
前一个禁用了行自动打印， 而后者使文件在原位编辑并且不产生备份，不小心使用的话， 输出 文件会是空的：  
>#WRONG USAGE: 'FILE' will be truncated.  
>sed -ni 's/foo/bar/' FILE


#### -l N
##### --line-length=N
指定 l 命令的默认行回绕长度，0表示永远不要回绕长行，如果 没有指定，使用70做为设定

#### --posix
相较于POSIX sed, GNU sed使用了多个扩展。  
为了简化 写可移植脚本，这个选项禁用了所有的本手册里记录的扩展，也包括额外命令。  
大部分的扩展（ accept sed programs that are outside the syntax）是得到了POSIX许可的，但是有一部分扩展会跟标准冲突（比如在bug反馈里提到 的N 命令的行为）。  
如果你只想禁用后一种扩展，你可以将 变量POSIXLY_CORRECT  设定为非空值 。

#### -b
##### --binary
这个选项适用于所有的平台，但是仅当操作系统会区别文本文件跟二进制文件的时候 才会有效果。  
当系统有这种区别的时候 (ms-DOS, Windows,Cygwin)，文本文件会被看成是由 CR和LF分隔的行构成，sed并不会看到结尾的CR
当这个选项被 指定，sed会以binary 模式打开输入文件，thus not requesting this special processing and considering lines to end at a line feed.

#### --follow-symlinks
这个选项只被使用在支持符号链接的平台上，也只在使用了-i选项的时候起作用。  
在这种情况下，如果命令中指定的文件是符号链接， sed会跟随链接，对链接的最终文件进行编辑。  
在默认情况下， sed会打断链接，所以链接 的目标文件是不会被修改的。

#### -E
##### -r
##### --regexp-extended
使用扩展的正则表达式面不是基本正则表达式。  
扩展正则表达式是 egrep 接收的正则表达式。  
扩展正则表达式会更清楚，因为他通达会使用更少的反斜杠。  
在过去这一点是GNU 扩展的部分，但是 -E扩展已经 被 加入到了POSIX标准（http://austingroupbugs.net/view.php?id=528），因此-E是可移植的。  
GNU sed已经 接收 -E做一个非标准选项多年， BSD sed 也接收 -E多年，但是使用了-E的脚本可能不能移植到一些更古老的系统

#### -s
##### --separate
默认情况下，sed把命令行里指定的所有文件看做是一个连续的流。  
这个GNU sed扩展请允许用户把他们看成是独立的文件：  
区间定位（）不请允许跨越多个文件，  
行号参照每个文件的开头。  
$ 指示每个文件的最后一行。
能过R命令调用 的文件在每个文件开始时回到开始（倒回）

#### --sandbox
在sandbox 模式里，**e/w/r/**命令将会被驳回-（包含这些命令的程序将会终止执行）。  
sandbox模式确保sed操作只会在 命令中指定的输入文件上运行，并且不会执行额外程序。
#### -u
##### --unbuffered
输入和输出 buffer应该尽可能的少。（如果输入是来自于像 'tail -f'并且你期望尽可能快的看到处理结果，这一点尤其重要）

#### -z
##### --null-data
##### --zero-terminated
把输入看成所有行的集合，每一行是用空字符结尾（'\0'分隔每一行）页不是换行符。  
这个选项可以像命令 'sort -z' 和'find -print0' 一样使用处理任意多个文件。
> （所有文件的）所有的输入行被连接在一起，变成一行文本。使用'\0'分隔每一行


如果命令里没有-e, -f, --expression 或--file 选项，命令里的第一个非选项参数会被录做脚本来运行。  
如果所有的参数经过上面的处理，还有剩余，它们就会被当做是输入文件来处理。  
'-' 指代的是标准输入。 
如果 没有指定任何输入文件，标准输入就会被当做输入。 

## 2.3 退出状态

返回码 0 代表命令成功结束，非0 返回码代表有错误产生。  
GNU sed会返回下面的代码做为错误码：

* 0
成功结束

* 1
非法命令，非法语法，非法正则表达式或者 GNU sed 扩展命令使用了 --posix

* 2
命令中指定 的一个或多个输入文件无法打开 （e.g. 如果文件存在 或没有读取权限）。命令会继续处理其它文件.

* 4
运行过程中出现 I/O 错误 或 处理错误， GNU sed会立即终止。

另外，命令 q 跟 Q 可以被用来终止 sed并返回一个指定 的返回代码（这是GNU sed的扩展）  
`$ echo | sed 'Q42' ; echo $?`  
`42`


[上一章](https://gitee.com/stand_back/sed-manual-translation/blob/master/content/chp1.md),[下一章](https://gitee.com/stand_back/sed-manual-translation/blob/master/content/chp3.md),[返回](https://gitee.com/stand_back/sed-manual-translation/blob/master/README.md)
