
# [第一章 简介](https://www.gnu.org/software/sed/manual/sed.html#Introduction)

sed是一个流编辑器， 
流编辑器是被用来对输入流（文件或管道输入）进行基本的文本转换。
在一定程度上跟基它提供脚本编辑能力的编辑器相似（比如ed), sed只对输入进行一次处理，相对的也更有效率.
sed可以对管道中的文本进行过滤处理是他区别与起来类型编辑器的主要特点。

[上一章](https://gitee.com/stand_back/sed-manual-translation/blob/master/README.md),[下一章](https://gitee.com/stand_back/sed-manual-translation/blob/master/content/chp2.md),[返回](https://gitee.com/stand_back/sed-manual-translation/blob/master/README.md)
