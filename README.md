# sed-manual-translation

#### 介绍
本仓库为[sed 用户手册](https://www.gnu.org/software/sed/manual/sed.html)翻译

GNU sed

本文档是版本为v4.8的流编辑器 GNU sed 的用户手册。

Copyright © 1998–2020 Free Software Foundation, Inc.

`Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled “GNU Free Documentation License”. `

* [简介](https://gitee.com/stand_back/sed-manual-translation/blob/master/content/chp1.md)          简介
* [运行sed](https://gitee.com/stand_back/sed-manual-translation/blob/master/content/chp2.md)				调用 sed
* [sed 脚本]()				sed 脚本
* [sed 定位]()				定位：行选择
* [sed 正则表达式]()		正则表达式：文本内容选择
* [sed 进阶技巧]()			进阶技巧： 工作循环 与 buffers
* [sed 实例]()				一些sed常用实例
* [sed 约束]()				GNU sed的约束与非约束
* [sed 学习资源]()			其它 一些学习sed的资源
* [sed bug 反馈]()			反馈 bugs
* [GNU 自由文档许可]()		复制和分享本文档
* [内容索引]()				本文档中所有内部的目录 
* [命令和选项索引]()		sed 所有命令和选项的目录
	
